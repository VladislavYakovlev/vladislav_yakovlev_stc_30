package ru.inno.sockets;

import ru.inno.sockets.repositories.*;
import ru.inno.sockets.server.GameServer;
import ru.inno.sockets.services.TanksServiceImpl;


public class Main {
    public static void main(String[] args) {
        // TODO: передать DataSource
        GamesRepository gamesRepository = new GamesRepositoryImpl();
        PlayersRepository playersRepository = new PlayersRepositoryImpl();
        ShotsRepository shotsRepository = new ShotsRepositoryImpl();
        GameServer gameServer = new GameServer(new TanksServiceImpl(shotsRepository, playersRepository, gamesRepository));
        gameServer.start(5432);
    }
}
