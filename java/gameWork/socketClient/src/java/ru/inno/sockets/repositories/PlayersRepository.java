package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;


public interface PlayersRepository {
    Player findByNickname(String nickname);

    void save(Player firstPlayer);
}
