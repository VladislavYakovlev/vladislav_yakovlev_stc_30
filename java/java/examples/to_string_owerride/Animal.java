package examples.to_string_owerride;

public class Animal {

    String name;
    int age;

    public Animal(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "object = " + super.toString() + " {name = " + name  + ": age = " + age + "}";
    }
}
