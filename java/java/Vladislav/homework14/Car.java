package Vladislav.homework14;

import java.util.Objects;

public class Car {
    private int number;
    private String model;
    private String color;
    private int km;
    private int price;

    public Car(int number, String model, String color, int km, int cost) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.km = km;
        this.price = cost;
    }

    public int getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getKm() {
        return km;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return number == car.number
                && km == car.km
                && price == car.price
                && Objects.equals(model, car.model)
                && Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, km, price);
    }

    @Override
    public String toString() {
        return "Car{" +
                "number=" + number +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", km=" + km +
                ", cost=" + price +
                '}';
    }
}
