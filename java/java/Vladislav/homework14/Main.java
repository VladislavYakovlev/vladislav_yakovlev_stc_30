package Vladislav.homework14;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    static Path path = Paths.get("java/cars.txt");

    public static List<Car> getCars(Path path) throws IOException {
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
            return Files.lines(path)
                    .map(line -> line.split(" "))
                    .peek(Main::validateLine)
                    .map(array -> new Car(Integer.parseInt(array[0]),
                            array[1],
                            array[2],
                            Integer.parseInt(array[3]),
                            Integer.parseInt(array[4]))).collect(Collectors.toList());
    }

    private static void validateLine(String[] str) {
        if (str.length != Car.class.getDeclaredFields().length) {
            throw new IllegalArgumentException("невозможно конвертировать {" + String.join(" ", str) + "} строку в машину" +
                    "строка должна иметь формат {id name model}");
        }
    }

    public static void main(String[] args) throws IOException {

        System.out.println("Список всех авто:");
        getCars(path).forEach(System.out::println);

        System.out.println("1. Номера всех автомобилей, имеющих черный цвет или нулевой пробег");
        getCars(path).stream()
                .filter(filter -> filter.getColor().equals("BLACK") || filter.getKm() == 0)
                .map(Car::getNumber)
                .forEach(entity -> System.out.print(entity + " "));
        System.out.println();

        System.out.println("2. Уникальные модели по цене от 700 до 800 тыс");
        int count = getCars(path)
                .stream()
                .filter(filter -> filter.getPrice() >= 700000 && filter.getPrice() <= 800000)
                .map(Car::getModel)
                .collect(Collectors.toSet()).size();
        System.out.println(count);

        System.out.println("3. Цвет автомобиля с минимальной стоимостью");
        getCars(path).stream()
                .min(Comparator.comparingInt(Car::getPrice))
                .ifPresent(System.out::println);

        System.out.println("4. Средняя стоимость Camry");
        System.out.println(getCars(path).stream()
                .filter(filter -> filter.getModel().equals("CAMRY")) // TODO форматирование
                .map(Car::getPrice)
                .mapToInt(Integer::intValue)
                .average().getAsDouble());
    }
}
