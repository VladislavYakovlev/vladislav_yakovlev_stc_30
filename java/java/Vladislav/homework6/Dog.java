package Vladislav.homework6;

public class Dog {
    //fields
    String name;
    int weight;
    String poroda;

    public Dog() {

    }

    public Dog(int weight) {
        this.weight = weight;
    }
    public Dog(int weight, String name, String poroda) {
        this.weight = weight;
        this.name = name;
        this.poroda = poroda;
    }

    @Override
    public String toString() {
        return name;
    }
}

