package Vladislav.homework6.tv;

public class RemoteController {

    public void turnOn(Television television) {
        television.turnOn();
    }

    public void pushButton(Television television, int buttonNumber) {
        television.turnChannel(buttonNumber);
    }

}
