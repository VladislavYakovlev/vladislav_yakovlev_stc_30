package Vladislav.homework6.tv;

public class Television {
    Channel[] channels;
    private String mark;
    boolean turnOn;

    public Television(String mark) {
        this.mark = mark;
        turnOn = false;
    }
    public void turnOn() {
       turnOn = true;
       System.out.println("This tv " + mark + " turned on");
    }
    public void turnOff() {
        turnOn = false;
        System.out.println("This tv " + mark + " turned off");
    }

    public void setChannels(Channel[] channels) {
        this.channels = channels;
        for(int i = 0; i < channels.length; i++) {
            System.out.println("Channel " + channels[i].getName() + " installed");
        }
    }

    public void turnChannel(int channel) {
        if ((channel - 1) >= 0 && channel <= channels.length) {
            System.out.println("Channel " + channels[channel - 1].getName() + " now turned ON!");
            // TODO тут должен доставаться контетнт из конала и выводиться в консоль
        } else {
            System.err.println("Incorrect channel!");
        }
      // String content = channels[channel - 1].getProgram();
      //  System.out.println(content);
    }
}
