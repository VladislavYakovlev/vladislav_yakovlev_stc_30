package Vladislav.homework6.tv;

public class Program {

    private String programName;
    private String content;

    public Program(String programName, String content) {
        this.programName = programName;
        this.content = content;
    }

    public String getProgramName() {
        return programName;
    }

    public String getContent() {
        return content;
    }
}
