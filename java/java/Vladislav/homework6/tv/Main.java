package Vladislav.homework6.tv;

public class Main {
    public static void main(String[] args) {
        Television tv1 = new Television("Samsung");
        Television tv2 = new Television("LG");
        Channel ch1 = new Channel("AnimalPlanet");
        Channel ch2 = new Channel("codingWorld");
        Channel ch3 = new Channel("Cartoon");
        Channel[] channels = {ch1, ch2, ch3};
        RemoteController pult = new RemoteController();

        tv1.setChannels(channels);

        pult.turnOn(tv1);

        pult.pushButton(tv1, 2);
    }
}
