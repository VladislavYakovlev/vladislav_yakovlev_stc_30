package Vladislav.homework10;

public interface List extends Collections {
    int getElement(int index);
    int indexOf(int element);
    void removeByIndex(int index);
    void insert(int index, int element);
}
