package Vladislav.homework10;

public interface Iterable {
    Iterator iterator();
}
