package Vladislav.homework10;

public class ArrayList implements List {

    private static final int DEFAULT_SIZE = 10;
    private int[] data;
    private int count;

    private class ArrayListIterator implements Iterator {

        private int current = 0;

        @Override
        public int next() {
            int value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    public ArrayList() {
        this.data = new int[DEFAULT_SIZE];
    }

    @Override
    public int getElement(int index) {
        if (index >=0 && index < count) {
            return this.data[index];
        } else {
            System.err.println("Вышли за пределы массива");
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public int indexOf(int element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count) {
            if (count - 1 - index >= 0) System.arraycopy(this.data,
                    index + 1,
                    this.data,
                    index,
                    count - 1 - index);
            count--;
        } else {
            System.err.println("Попытка удаления по индексу которого в списке нет");
        }
    }

    @Override
    public void insert(int index, int element) {
        if (index >= 0 && index < count) {
            int changeElement = data[index];
            for (int i = index; i < count; i++) {
                int newChangeElement = data[i + 1];
                data[i + 1] = changeElement;
                changeElement = newChangeElement;
            }
            data[index] = element;
            count++;
        } else {
            System.err.println("Индекс выходит за пределы размера списка");
        }
    }

    @Override
    public void add(int element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1);  // N >> K = N / 2^K
        int[] newData = new int[newLength];

        System.arraycopy(data, 0, newData, 0, oldLength);
        this.data = newData;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(int element) {
        if (indexOf(element) != -1) {
            int indexOfRemovingElement = indexOf(element);
            if (count - 1 - indexOfRemovingElement >= 0)
                System.arraycopy(this.data,
                        indexOfRemovingElement + 1,
                        this.data,
                        indexOfRemovingElement,
                        count - 1 - indexOfRemovingElement);
            count--;
        } else {
            System.err.println("Попытка удаления элемента которого в списке нет");
        }
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}
