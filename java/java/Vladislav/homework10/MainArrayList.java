package Vladislav.homework10;

import java.util.Arrays;

public class MainArrayList {
    public static void main(String[] args) {
        List list = new ArrayList();

        for (int i = 0; i < 17; i++) {
            list.add(i);
        }

        System.out.println("Всего элементов: " + list.size());

        list.removeByIndex(13);

        list.insert(15, 228);

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("Всего элементов: " + list.size());
    }
}
