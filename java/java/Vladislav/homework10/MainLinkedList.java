package Vladislav.homework10;

public class MainLinkedList {
    public static void main(String[] args) {
        List list = new LinkedList();

        for (int i = 1; i < 17; i++) {
            list.add(i);
        }
        System.out.println(list.contains(11));

        list.removeByIndex(1);
        list.removeFirst(13);
        list.insert(1, 1337);

        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("Всего элементов: " + list.size());
    }
}
