package Vladislav.homework10;

public class LinkedList implements List {

    private Node first;
    private Node last;
    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    private class LinkedListIterator implements Iterator {

        private int current = 0;

        @Override
        public int next() {
            int value = getElement(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public int getElement(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Такого элемента нет");
        throw new IndexOutOfBoundsException();
    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    private boolean isValidIndex(int index) {
        if (index >= 0 && index < count) return true;
        else {
            System.err.println("Неверно заданный индекс");
            return false;
        }
    }

    private Node getPreviousNodeByIndex(int index) {
        Node current = this.first;
        if (index != 1) {
            int i = 1;
            while (i < index - 1) {
                current = current.next;
                i++;
            }
        }
        return current;
    }

    @Override
    public void removeByIndex(int index) {
        if (isValidIndex(index)) {
            Node current = getPreviousNodeByIndex(index);
            if (index == 1) {
                this.first = current.next;
            } else {
                current.next = current.next.next;
            }
            count--;
        }
    }

    @Override
    public void insert(int index, int element) {
        if (isValidIndex(index)) {
            Node current = getPreviousNodeByIndex(index);
            Node newNode = new Node(element);
            if (index == 1) {
                newNode.next = this.first;
                this.first = newNode;
            } else {
                newNode.next = current.next;
                current.next = newNode;
            }
            count++;
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (this.first == null) {
            this.first = newNode;
        } else {
            last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return count;
    }

    private Node getPreviousNodeByElement(int element) {
        Node current = this.first;
        if (element != current.value) {
            while (element != current.next.value) {
                current = current.next;
            }
        }
        return current;
    }

    @Override
    public void removeFirst(int element) {
        Node current = getPreviousNodeByElement(element);
        current.next = current.next.next;
        count--;
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
