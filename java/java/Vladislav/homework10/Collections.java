package Vladislav.homework10;

public interface Collections extends Iterable {
    void add(int element);
    boolean contains(int element);
    int size();
    void removeFirst(int element);
}
