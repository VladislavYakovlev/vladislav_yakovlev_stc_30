package Vladislav.homework11;

public interface List<T> extends Collections<T> {
    T getElement(int index);
    int indexOf(T element);
    void removeByIndex(int index);
    void insert(int index, T element);
}
