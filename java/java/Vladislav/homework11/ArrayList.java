package Vladislav.homework11;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

    private static final int DEFAULT_SIZE = 10;
    private T data[];
    private int count;

    private class ArrayListIterator<T> implements Iterator<T> {

        private int current = 0;

        @Override
        public T next() {
            T value = (T) data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    public ArrayList() {
        this.data = (T[]) new Object[DEFAULT_SIZE];
    }

    @Override
    public T getElement(int index) {
        if (index >=0 && index < count) {
            return this.data[index];
        } else {
            System.err.println("Вышли за пределы массива");
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (data[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count) {
            if (count - 1 - index >= 0) System.arraycopy(this.data,
                    index + 1,
                    this.data,
                    index,
                    count - 1 - index);
            count--;
        } else {
            System.err.println("Попытка удаления по индексу которого в списке нет");
        }
    }

    @Override
    public void insert(int index, T element) {
        if (index >= 0 && index < count) {
            T changeElement = data[index];
            for (int i = index; i < count; i++) {
                T newChangeElement = data[i + 1];
                data[i + 1] = changeElement;
                changeElement = newChangeElement;
            }
            data[index] = element;
            count++;
        } else {
            System.err.println("Индекс выходит за пределы размера списка");
        }
    }

    @Override
    public void add(T element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1);  // N >> K = N / 2^K
        T newData[] = (T[]) new Object[newLength];

        System.arraycopy(data, 0, newData, 0, oldLength);
        this.data = newData;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(T element) {
        if (contains(element)) {
            int indexOfRemovingElement = indexOf(element);
            if (count - 1 - indexOfRemovingElement >= 0)
                System.arraycopy(this.data,
                        indexOfRemovingElement + 1,
                        this.data,
                        indexOfRemovingElement,
                        count - 1 - indexOfRemovingElement);
            count--;
        } else {
            System.err.println("Попытка удаления элемента которого в списке нет");
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArrayList)) return false;
        ArrayList<?> arrayList = (ArrayList<?>) o;
        return Arrays.equals(data, arrayList.data); // TODO проверка не должна зависиеть от размера вложенного массива
    }
}
