package Vladislav.homework11;

public class LinkedList<T> implements List<T> {

    private Node<T> first;
    private Node<T> last;
    private int count;

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private class LinkedListIterator<T> implements Iterator<T> {

        private int current = 0;

        @Override
        public T next() {
            T value = (T) getElement(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public T getElement(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<T> current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Такого элемента нет");
        throw new IndexOutOfBoundsException();
    }

    @Override
    public int indexOf(T element) {
        int i = 0;
        Node<T> current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    private boolean isValidIndex(int index) {
        if (index >= 0 && index < count) return true;
        else {
            System.err.println("Неверно заданный индекс");
            return false;
        }
    }

    private Node<T> getPreviousNodeByIndex(int index) {
        Node<T> current = this.first;
        if (index != 1) {
            int i = 1;
            while (i < index - 1) {
                current = current.next;
                i++;
            }
        }
        return current;
    }

    @Override
    public void removeByIndex(int index) {
        if (isValidIndex(index)) {
            Node<T> current = getPreviousNodeByIndex(index);
            if (index == 1) {
                this.first = current.next;
            } else {
                current.next = current.next.next;
            }
            count--;
        }
    }

    @Override
    public void insert(int index, T element) {
        if (isValidIndex(index)) {
            Node<T> current = getPreviousNodeByIndex(index);
            Node<T> newNode = new Node<>(element);
            if (index == 1) {
                newNode.next = this.first;
                this.first = newNode;
            } else {
                newNode.next = current.next;
                current.next = newNode;
            }
            count++;
        }
    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (this.first == null) {
            this.first = newNode;
        } else {
            last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return count;
    }

    private Node<T> getPreviousNodeByElement(T element) {
        Node<T> current = this.first;
        if (element != current.value) {
            while (element != current.next.value) {
                current = current.next;
            }
        }
        return current;
    }

    @Override
    public void removeFirst(T element) {
        Node<T> current = getPreviousNodeByElement(element);
        current.next = current.next.next;
        count--;
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator<>();
    }
}
