package Vladislav.homework11;

public interface Iterable<T> {
    Iterator<T> iterator();
}