package Vladislav.homework11;

public interface Iterator<T> {
    // возвращает следующий элемент
    T next();
    // проверяет есть ли следующий элемент
    boolean hasNext();
}
