package Vladislav.homework11;

public interface Collections<T> extends Iterable<T> {
    void add(T element);
    boolean contains(T element);
    int size();
    void removeFirst(T element);
}
