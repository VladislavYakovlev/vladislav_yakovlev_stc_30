package Vladislav.homework3;

import java.util.Scanner;
import java.util.Arrays;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter array size:");
		int size = scanner.nextInt();
		int[] array = new int[size];
		System.out.println("Please write the numbers included the array:");
	
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		int[] revertArray = array.clone();
		int[] swapArray = array.clone();
		int[] convertArray = array.clone();
		System.out.println(Arrays.toString(array));
		System.out.println("1) Sum of array numbers = " + sumOfArray(array));
		System.out.println("2) Revert array: " + Arrays.toString(revert(revertArray)));
		System.out.println("3) Average arithmetical of array = " + average(array));
		System.out.println("4) Swapped min and max values of array: " + Arrays.toString(swap(swapArray)));
		System.out.println("5) Converted array: " + Arrays.toString(sort(convertArray)));
		System.out.println("6) Full number of array: " + join(array));
	}
	// Программа суммы массива
	public static int sumOfArray(int[] a) {
		int arraySum = 0;
		for(int i = 0; i < a.length; i++) {
				arraySum += a[i];
		}
		return arraySum;
	}
	// развернутый массив
	public static int[] revert(int[] a) {
		for (int i = 0; i < (a.length - i - 1); i++) {
			int t = a[i];
			a[i] = a[a.length - i - 1];
			a[a.length - i - 1] = t;
		}
		return a;
	}
	public static double average(int[] a) {
		double sum = 0;
		for( int i = a.length - 1; i >= 0; i--) {
			sum = sum + a[i];
		}
		double arrayAverage = sum / a.length;
		return arrayAverage;
	}
	public static int[] swap(int[] a) {
		int min = a[0];
		int indexOfMin = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] < min) {
				min = a[i];
				indexOfMin = i;
			}
		}
		int max = a[0];
		int indexOfMax = 0;
		for (int j = 0; j < a.length; j++) {
			if (a[j] > max) {
				max = a[j];
				indexOfMax = j;
			}
		}
		int change = a[indexOfMin];
		a[indexOfMin] = a[indexOfMax];
		a[indexOfMax] = change;
		return a;
	}
	public static int[] sort(int[] a) {
		for(int x = 1; x < a.length; x++) {
			for(int y = a.length - 1; y >= x; y--) {
				if(a[y-1] > a[y]) {
					int t = a[y-1];
					a[y-1] = a[y];
					a[y] = t;
				}
			}
		}
		return a;
	}
	public static double join(int[] array) {
		double number = 0;
		for (int i = array.length - 1; i >= 0; i--) {
			double a = 0;
			a = Math.pow(10, (array.length - (i + 1)));
			number += array[i] * a;
		}
		return number;
	}
}