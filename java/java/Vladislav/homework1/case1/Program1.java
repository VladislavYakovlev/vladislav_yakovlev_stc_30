package Vladislav.homework1.case1;

class Program1 {
	public static void main(String[] args)	{
		int number = 49281;
		int a = number / 10000;
		int b = number / 1000 - (a * 10);
		int c = number / 100 - ((a * 100) + (b * 10));
		int d = number / 10 - ((a * 1000) + (b * 100) + (c * 10));
		int e = number - ((a * 10000) + (b * 1000) + (c * 100) + (d * 10));
		int digitsSum = a + b + c + d + e;
		System.out.println("Sum of numbers " + a + " + " + b + " + " + c + " + " + d + " + " + e + " = " + digitsSum);
	}
}