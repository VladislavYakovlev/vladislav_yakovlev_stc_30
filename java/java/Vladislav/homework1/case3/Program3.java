package Vladislav.homework1.case3;

import java.util.Scanner;

class Program3 {
	public static void main(String[] args)	{
		Scanner scanner = new Scanner(System.in);
		int inputNumber = scanner.nextInt();
		int multiplicationInputNumbers = 1; 
		
		while (inputNumber > 0) { 
			int anotherNumber = inputNumber;
			int sum = 0;
			while (anotherNumber > 0) {  
				int x = anotherNumber % 10;
				sum = sum + x;
				anotherNumber = anotherNumber / 10;
			}
			int check = sum - 1; 
			boolean isComposite = false;
		
			while (check > 1) {  
				isComposite = sum % check == 0;
				if (isComposite) {
					break;
				} else {
					check = check - 1;
				}
			}
			if (isComposite) {
				System.out.println( inputNumber + "(sum of numbers - " + sum + ", composite number)");
			} else {
				System.out.println( inputNumber + "(sum of numbers - " + sum + ", simple number)");
				multiplicationInputNumbers = multiplicationInputNumbers * inputNumber;
			}
			inputNumber = scanner.nextInt();
		}
		System.out.println(multiplicationInputNumbers);
	}
}