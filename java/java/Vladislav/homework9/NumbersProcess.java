package Vladislav.homework9;

public interface NumbersProcess {
    public int process(int number);
}
