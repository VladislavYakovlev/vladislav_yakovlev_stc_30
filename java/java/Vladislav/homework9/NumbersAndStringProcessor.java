package Vladislav.homework9;

public class NumbersAndStringProcessor {
    private String[] strings;
    private int[] numbers;

    public NumbersAndStringProcessor(String[] strings, int[] numbers) {
        this.strings = strings;
        this.numbers = numbers;
    }
    public String[] process(StringsProcess stringsProcess) {
        String[] newArray = new String[strings.length];
        for (int i = 0; i < strings.length; i++) {
            newArray[i] = stringsProcess.process(strings[i]);
        }
        return newArray;
    }

    public int[] process(NumbersProcess numbersProcess) {
        int[] newArray = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            newArray[i] = numbersProcess.process(numbers[i]);
        }
        return newArray;
    }
}
