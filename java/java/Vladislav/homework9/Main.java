package Vladislav.homework9;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] arrayNum = {12003450, 7608228, 1337, 228, 840921};
        String[] arrayStrings = {"При1ве3т", "Что 2де3ла4ешь", "Мн3е по0ра"};

        NumbersAndStringProcessor processor = new NumbersAndStringProcessor(arrayStrings, arrayNum);

        NumbersProcess reverseNumber = number -> {
            int result = 0;
            while(number > 0) {
                result = result * 10 + number % 10;
                number /= 10;
            }
            return result;
        };

//        second way
//        NumbersProcess reverseNumber = number -> {
//            String num = String.valueOf(number);
//            String reverseString = inverter.process(num);
//            int reverseNum = Integer.parseInt(reverseString);
//            return reverseNum;
//        };

        NumbersProcess zeroCutter = number -> {
            int result = 0;
            int multiplier = 1;
            while (number != 0) {
                result += (number % 10) * multiplier;
                if (number % 10 != 0) {
                    multiplier *= 10;
                }
                number /= 10;
            }
            return result;
        };

        NumbersProcess switchOddToEven = number -> {
            int result = 0;
            int multiplier = 1;
            while (number != 0) {
                int checkEven = number % 10;
                if (checkEven % 2 == 0) {
                    result += checkEven * multiplier;
                } else {
                    result += (checkEven - 1) * multiplier;
                }
                multiplier *= 10;
                number /= 10;
            }
            return result;
        };

        StringsProcess inverter = oldString -> {
            String result = "";
            char[] chars = oldString.toCharArray();
            for (int i = chars.length - 1; i >= 0; i--) {
                result += chars[i];
            }
            return result;
        };

        StringsProcess stringToUpperCase = String::toUpperCase;

        StringsProcess numbersCutter = string -> {
            char[] str = string.toCharArray();
            char[] newString = new char[str.length];
            int counter = 0;
            for (char i : str) {
                if (Character.isLetter(i) || Character.isSpaceChar(i)) {
                    newString[counter] = i;
                    counter++;
                }
            }
            return String.valueOf(newString, 0, counter);
        };

        System.out.println("First array of numbers: " + Arrays.toString(arrayNum));
        System.out.println("Reversed array: " + Arrays.toString(processor.process(reverseNumber)));
        System.out.println("Removes zeros from array: " + Arrays.toString(processor.process(zeroCutter)));
        System.out.println("Switch odd numbers to even: " + Arrays.toString(processor.process(switchOddToEven)));
        System.out.println();
        System.out.println("First array of strings: " + Arrays.toString(arrayStrings));
        System.out.println("Inverts strings: " + Arrays.toString(processor.process(inverter)));
        System.out.println("String to upper case: " + Arrays.toString(processor.process(stringToUpperCase)));
        System.out.println("Removes numbers from string: " + Arrays.toString(processor.process(numbersCutter)));

    }
}

//        StringsProcess spaceDropper = string -> {
//            String result = "";
//            char[] chars = string.toCharArray();
//            for (char aChar : chars) {
//                if (aChar != ' ') {
//                    result += aChar;
//                }
//            }
//            return result;
//        };
//
//        StringsProcess stringCutter = string -> string.substring(0, 10);
//
//        StringsProcess[] objects =
//                {stringPrinter,
//                        stringToUpperCase,
//                        inverter,
//                        stringPrinter,
//                        stringCutter,
//                        spaceDropper,
//                        stringPrinter};
//
//        processor.process(objects, "try to do something new");
