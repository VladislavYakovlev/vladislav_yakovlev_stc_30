package Vladislav.homework7;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public static class Builder {
        private String firstName = "";
        private String lastName = "";
        private int age = 0;
        private boolean isWorker = false;

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public Builder setWorker(boolean worker) {
            this.isWorker = worker;
            return this;
        }
        public User build() {
            return new User(this);
        }
    }
    private User (Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.isWorker = builder.isWorker;
    }
}
