package Vladislav.homework7;

public class Main {
    public static void main(String[] args) {
        User user = new User.Builder()
                .setFirstName("Vladislav")
                .setLastName("Yakovlev")
                .setAge(10)
                .setWorker(true)
                .build();

        System.out.println("User first name " + user.getFirstName()
                + " last name " + user.getLastName()
                + " with age " + user.getAge()
                + " now is working = " + user.isWorker());
    }
}
