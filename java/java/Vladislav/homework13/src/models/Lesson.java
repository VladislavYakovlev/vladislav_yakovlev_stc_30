package Vladislav.homework13.src.models;

import java.time.LocalDateTime;

public class Lesson {
    String name;
    LocalDateTime eventDate;
    Course course;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Lesson(String name, LocalDateTime eventDate, Course course) {
        this.name = name;
        this.eventDate = eventDate;
        this.course = course;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "name='" + name + '\'' +
                ", eventDate=" + eventDate +
                ", course=" + course +
                '}';
    }
}
