package Vladislav.homework13.src.models;

import java.time.LocalDateTime;
import java.util.List;

public class Course {
    int id;
    String name;
    LocalDateTime startTime;
    LocalDateTime endTime;
    List<Teacher> teachers; // TODO здесь сохранять не полностью объекты teacher а только их id
    List<Lesson> lessons;

    public Course(int id, String name, LocalDateTime startTime, LocalDateTime endTime, List<Teacher> teachers, List<Lesson> lessons) {
        this.id = id;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.teachers = teachers;
        this.lessons = lessons;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", teachers=" + teachers +
                ", lessons=" + lessons +
                '}';
    }
}
