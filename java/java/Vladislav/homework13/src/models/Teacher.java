package Vladislav.homework13.src.models;

import java.util.List;

public class Teacher {
    String name;
    String surName;
    int experience;
    List<Course> courses;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Teacher(String name, String surName, int experience, List<Course> courses) {
        this.name = name;
        this.surName = surName;
        this.experience = experience;
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", experience=" + experience +
                ", courses=" + courses +
                '}';
    }
}
