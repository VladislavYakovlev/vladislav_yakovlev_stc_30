package Vladislav.homework12;

public class HashSet<V> implements Set<V> {
    private HashMap<V, Object> hashMap = new HashMap<>();
    private static final Object PLUG = new Object();

    @Override
    public void add(V value) {
        hashMap.put(value, PLUG);
    }

    @Override
    public boolean contains(V value) {
        return hashMap.contains(value);
    }
}
