package Vladislav.homework12;


public class HashMap<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }


    // put("Марсель", 27);
    // put("Марсель", 28);
    // у меня сохранятся оба значения, вам нужно сделать, чтобы хранилось только одно
    @Override
    public void put(K key, V value) { // TODO нужно проработать декомпозицию
        int index = getIndex(key);
        if (entries[index] == null) {
            entries[index] = newMapEntry(key, value);
        } else {
            workWithTheSameHash(key, value, index); // TODO методы с заваниями work, process, do не несут пользы
        }
        System.out.println("У объекта " + key + " hash = " + index);  //проверочка (для себя)
    }

    /**
     * This method works if we have the same hashcode. If the added key is equivalent to
     * the key that already exists, then we change the value to a new one.
     * If our key is unique, then we add it to the end.
     * @param key key
     * @param value value
     * @param index hashcode
     */
    private void workWithTheSameHash(K key, V value, int index) {
        MapEntry<K, V> current = entries[index];
        if (current.key.equals(key)) {
            current.value = value;
        } else {
            while (current.next != null && !current.key.equals(key)) {
                current = current.next;
            }
            if (current.key.equals(key)) {
                current.value = value;
            } else {
                current.next = newMapEntry(key, value);
            }
        }
    }

//    public void newPut(K key, V value) {
//        MapEntry<K,V> entry = findEntry(key);
//        if (entry != null) {
//            entry.value = value;
//        } else {
//            addEntry(newMapEntry(key, value));
//        }
//    }
//
//    private void addEntry(MapEntry<K,V> newMapEntry) {
//        int index = getIndex(newMapEntry.key);
//        MapEntry<K, V> temp = entries[index];
//        newMapEntry.next = temp;
//        entries[index] = newMapEntry;
//    }
//
//    private MapEntry<K, V> findEntry(K key) {
//        int index = getIndex(key);
//        MapEntry<K, V> entry = entries[index];
//        while (!entry.key.equals(key)) {
//            entry = entry.next;
//        }
//        return entry;
//    }

    private MapEntry<K, V> newMapEntry(K key, V value) {
        return new MapEntry<>(key, value);
    }

    private int getIndex(K key) {
        return key.hashCode() & (entries.length - 1);
    }

    @Override
    public V get(K key) {
//        MapEntry<K, V> entry = findEntry(key);
//        if (entry != null) {
//            return entry.value;
//        } else {
//            return null;
//        }
        int index = getIndex(key);
        if (entries[index] == null) {
            System.err.println("Incorrect key!");
            return null;
        }
        MapEntry<K, V> current = entries[index];
        if (current.key.equals(key)) {
            return current.value;
        }
        while (current.next != null && !current.key.equals(key)) {
            current = current.next;
        }
        if (current.key.equals(key)) {
            return current.value;
        } else {
            System.err.println("incorrect key!");
            return null;
        }
    }

    public boolean contains(K key) {
        return get(key) != null;
    }
}
