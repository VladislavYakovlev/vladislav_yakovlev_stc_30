package Vladislav.homework15;

public class FastSort {

    public void quickSort(char array[]) {
        quickSortRealize(array, 0, array.length - 1);
    }

    private static void quickSortRealize(char array[], int left, int right) {
        int leftCounter = left;
        int rightCounter = right;
        char middleKey = array[(left+right)/2];

        do {
            while ((array[leftCounter] < middleKey) && (leftCounter < right)) {
                leftCounter++;
            }
            while ((middleKey < array[rightCounter]) && (rightCounter > left)) {
                rightCounter--;
            }
            if (leftCounter <= rightCounter) {
                char switcher = array[leftCounter];
                array[leftCounter] = array[rightCounter];
                array[rightCounter] = switcher;
                leftCounter++;
                rightCounter--;
            }
        } while(leftCounter <= rightCounter);

        if (left < rightCounter) {
            quickSortRealize(array, left, rightCounter);
        }
        if (leftCounter < right) {
            quickSortRealize(array, leftCounter, right);
        }
    }
}
