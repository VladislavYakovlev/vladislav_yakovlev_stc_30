package Vladislav.homework15;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //test mode
        char array[] = {'m', 'p', 'q', 'r', 'u', 'a', 'g', 'b'};
        System.out.println("old array^ " + Arrays.toString(array));

        FastSort fastSort = new FastSort();
        fastSort.quickSort(array);

        System.out.println("new sorted array^ " + Arrays.toString(array));
    }
}
