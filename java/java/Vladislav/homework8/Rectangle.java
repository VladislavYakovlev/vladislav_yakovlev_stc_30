package Vladislav.homework8;

public class Rectangle extends Figure {
    protected double length;
    private double height;

    public Rectangle(int centerX, int centerY, double length, double height) {
        super(centerX, centerY);
        this.length = length;
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double area() {
        return length * height;
    }

    @Override
    public double perimeter() {
        return (length + height) * 2;
    }

    @Override
    public void toScale(double count) {
        this.height = this.height * count;
        this.length = this.length * count;
    }
}
