package Vladislav.homework8;

public class Square extends Rectangle {

    public Square(int centerX, int centerY, double length) {
        super(centerX, centerY, length, length);
    }

    @Override
    public double area() {
        return length * length;
    }

    @Override
    public double perimeter() {
        return super.perimeter();
    }

    @Override
    public double getLength() {
        return super.length;
    }
}
