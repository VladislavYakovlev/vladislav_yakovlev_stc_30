package Vladislav.homework8;

public abstract class Figure implements Scalable, Relocatable {
    private int centerX;
    private int centerY;

    public Figure(int centerX, int centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }


    public abstract double area();
    public abstract double perimeter();

    @Override
    public void relocate(int newCenterX, int newCenterY) {
        this.centerX = newCenterX;
        this.centerY = newCenterY;
    }

    @Override
    public void toScale(double count) {

    }

    public int getCenterX() {
        return centerX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }
}
