package Vladislav.homework8;

public interface Relocatable {

    void relocate(int newCenterX, int newCenterY);
}
