package Vladislav.homework8;

public class Ellipse extends Figure {
    protected double radius;
    private double radius2;

    public Ellipse(int centerX, int centerY, double radius, double radius2) {
        super(centerX, centerY);
        this.radius = radius;
        this.radius2 = radius2;
    }

    @Override
    public double area() {
        return 1;
    }

    @Override
    public double perimeter() {
        return 0;
    }

    @Override
    public void toScale(double index) {

    }

}
