package Vladislav.homework8;

public class Circle extends Ellipse {

    public Circle(int centerX, int centerY, double radius) {
        super(centerX, centerY, radius, radius);
    }

    @Override
    public double area() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public double perimeter() {
        return Math.PI * radius * 2;
    }
}
