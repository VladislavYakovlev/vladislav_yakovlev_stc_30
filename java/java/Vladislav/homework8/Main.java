package Vladislav.homework8;


public class Main {
    public static void main(String[] args) {
        Figure f1 = new Circle(0, 0, 11);
        Figure f2 = new Ellipse(0, 0, 15, 15);
        Figure f3 = new Rectangle(0, 0, 5, 7);
        Figure f4 = new Square(0, 0, 8);

        Figure[] figures = {f1, f2, f3, f4};

        for (int i = 0; i < figures.length; i++) {
            int x = i + 1;
            System.out.println("area f" + x + " = " + figures[i].area());
            System.out.println("perimeter f" + x + " = " + figures[i].perimeter());
        }
        
        f3.toScale(2);
        f4.toScale(0.5);

//        System.out.println("new f3 length " + f3.getLength());
//        System.out.println("new f4 length " + f4.getLength());

        f1.relocate(1, 2);
        f2.relocate(3, 4);
        f3.relocate(5,6);
        f4.relocate(7,8);

        printLocation(f1, f2, f3, f4);
    }

    public static void printLocation(Figure... geometricFigure) {
        for (Figure figure : geometricFigure) {
            System.out.println(figure + " coordinate X: " + figure.getCenterX() + ", coordinate Y: " + figure.getCenterY());
        }
    }
}
//    public static void relocateAll(Relocatable[] objects) {
//        for (Relocatable object : objects) {
//            object.relocate();
//        }
//    }
//    public static void printArea(Figure... geometricFigure) {
//        for (Figure figure : geometricFigure) {
//            figure.area();
//        }
