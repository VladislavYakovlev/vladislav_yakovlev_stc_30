package Vladislav.homework2.case4;

import java.util.Scanner;
import java.util.Arrays;

class Program4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter array size:");
		int size = scanner.nextInt();
		int array[] = new int[size];
		
		System.out.println("Please write the numbers included the array:");
		for(int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println(Arrays.toString(array));
		
		//определяем минимальное значение
		int min = array[0];
		int indexOfMin = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
				indexOfMin = i;
			}
		}
		//определяем максимальное значение
		int max = array[0];
		int indexOfMax = 0;
		for (int j = 0; j < array.length; j++) {
			if (array[j] > max) {
				max = array[j];
				indexOfMax = j;
			}
		}
		//замена максимального и минимального значений
		int change = array[indexOfMin];
		array[indexOfMin] = array[indexOfMax];
		array[indexOfMax] = change;
		System.out.println(Arrays.toString(array));
	}
}