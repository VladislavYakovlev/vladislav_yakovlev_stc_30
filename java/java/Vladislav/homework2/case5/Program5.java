package Vladislav.homework2.case5;

import java.util.Arrays;

class Program5 {
	public static void main(String[] args) {
		int array[] = {118, -14, 103456, -754, 23, 1488, 364, 228, 1337, 19};
		int a, b, t;
		System.out.println(Arrays.toString(array));
		
		for(a = 1; a < array.length; a++) {
			for(b = array.length - 1; b >= a; b--) {
				if(array[b-1] > array[b]) {
					t = array[b-1];
					array[b-1] = array[b];
					array[b] = t;
				}
			}
		}
		System.out.println("Converted array: " + Arrays.toString(array));
	}
}