package Vladislav.homework2.case6;

import java.util.Scanner;
import java.util.Arrays;

class Program6 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter array size:");
		int size = scanner.nextInt();
		int array[] = new int[size];
		System.out.println("Please write the numbers included the array:");
	
		for(int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println(Arrays.toString(array));
		
		double number = 0;
		for (int i = array.length - 1; i >= 0; i--) {
			double a = 0;
			a = Math.pow(10, (array.length - (i + 1)));
			number = number + array[i] * a;
		}
		System.out.println("full number of array: " + number);
	}
}