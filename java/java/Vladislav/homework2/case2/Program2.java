package Vladislav.homework2.case2;

import java.util.Scanner;
import java.util.Arrays;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter array size:");
		int size = scanner.nextInt();
		int array[] = new int[size];
		System.out.println("Please write the numbers included the array:");
	
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println(Arrays.toString(array));
		
		for (int i = 0; i < (array.length - i - 1); i++) {
			int t = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = t;
		}
		System.out.println(Arrays.toString(array));
	}
}