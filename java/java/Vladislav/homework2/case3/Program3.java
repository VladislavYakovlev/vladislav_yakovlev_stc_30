package Vladislav.homework2.case3;

import java.util.Scanner;
import java.util.Arrays;

class Program3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter array size:");
		int size = scanner.nextInt();
		int array[] = new int[size];
		System.out.println("Please write the numbers included the array:");
	
		for(int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println(Arrays.toString(array));
		
		double sum = 0;
		for( int i = array.length - 1; i >= 0; i--) {
			sum = sum + array[i];
		}
		double arrayAverage = sum / array.length;
		System.out.println("Average arithmetical of array = " + arrayAverage);
	}
}