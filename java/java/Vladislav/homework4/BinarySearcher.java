package Vladislav.homework4;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearcher {
    public static void main(String[] args) {
        int[] array = inputArray();
        int[] sortedArray = sort(array.clone());
        System.out.println("Converted array: " + Arrays.toString(sortedArray));
        System.out.println(binarySearch(sortedArray, 10));
    }
    public static int[] inputArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter array size:");
        int size = scanner.nextInt();
        int[] array = new int[size];
        System.out.println("Please write the numbers included the array:");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    /**
     * this method from bynarySearch
     * @param array - array there we search
     * @param element - value that we need to find
     * @return index of element if find, -1 if dont find
     */
    public static int binarySearch(int[] array, int element) {
        return binarySearch(array, element, 0, array.length);
    }

    private static int binarySearch(int[] array, int element, int left, int right) {
        int mid = (right + left) / 2;
        if (array[mid] == element) {
            return mid;
        } else if (array[mid] < element) {
            left = mid + 1;
            return binarySearch(array, element, left, right);
        } else {
            right = mid - 1;
            return binarySearch(array, element, left, right);
        }
    }
    //Sort array
    private static int[] sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int indexOfArray = array.length - 1; indexOfArray >= i; indexOfArray--) {
                if (array[indexOfArray - 1] > array[indexOfArray]) {
                    int betterNumber = array[indexOfArray - 1];
                    array[indexOfArray - 1] = array[indexOfArray];
                    array[indexOfArray] = betterNumber;
                }
            }
        }
        return array;
    }
}